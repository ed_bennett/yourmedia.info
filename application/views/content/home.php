<div class="panel-container panel-flex panel-search">

    <div class="form-container">

        <h1>YourMediaDB</h1>
        <span class="tagline">Start your search here!</span>

        <?php if($this->session->flashdata('message') != '') ?>
            <span class="message"><?php echo $this->session->flashdata('message'); ?></span>
        <?php  ?>

        <span class="message"><?php echo validation_errors(); ?></span>

        <?php echo form_open('search/results'); ?>

            <label>Search:</label>
            <input type="text" name="media" id="media" />

            <div class="submit-container">
                <button type="submit" value="Search" name="submit">Search</button>
                <a href="<?php echo base_url() ?>admin/login"><i class="fa fa-cogs" aria-hidden="true"></i> Login to Dashboard</a> <a href="<?php echo base_url() ?>search/advanced"><i class="fa fa-search" aria-hidden="true"></i> Advaned Search</a>
            </div>

        <?php echo form_close(); ?>

    </div>

</div>
