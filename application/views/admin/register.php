<div class="panel-container panel-flex panel-search">

    <?php if(isset($this->session->userdata['logged_in'])) redirect('admin/dashboard'); ?>

    <div class="form-container">

        <?php if($this->session->flashdata('message') != '') ?>
            <span class="message"><?php echo $this->session->flashdata('message'); ?></span>
        <?php  ?>

        <span class="message"><?php echo validation_errors(); ?></span>

        <h1>Register a user</h1>

        <?php echo form_open('admin/register_setup'); ?>

            <label>Username :</label>
            <input type="text" name="username" id="name" /><br /><br />

            <label>Password :</label>
            <input type="password" name="password" id="password" /><br/><br />

            <div class="submit-container">
                <button type="submit" value="Register" name="submit">Register!</button>
                <a href="<?php echo base_url() ?>admin/login"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Return to login</a>
            </div>

        <?php echo form_close(); ?>

    </div>

</div>
