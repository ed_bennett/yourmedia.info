<?php

class Home extends CI_Controller {

    public function index() {
        $data = array('title' => 'MediaDB');
        $this->load->view('templates/header', $data);
        $this->load->view('content/home');
        $this->load->view('templates/footer');
    }

}
