<?php

class Setup extends CI_Controller {

    public function __constructor() {
        parent::__constructor;
    }

    public function stepone() {

        $this->load->dbforge();

        $fields = array(
                'movie_id' => array(
                        'type' => 'INT',
                        'constraint' => 5,
                        'unsigned' => TRUE,
                        'auto_increment' => TRUE
                ),
                'movie_name' => array(
                        'type' => 'MEDIUMTEXT'
                ),
                'movie_release' => array(
                        'type' =>'DATE'
                ),
                'movie_text' => array(
                        'type' => 'LONGTEXT',
                        'null' => TRUE,
                ),
        );

        $this->dbforge->add_field($fields);

        if($this->dbforge->create_table('eds_mdb', true)) :

            $this->session->set_flashdata('message', 'Database tables created successfully!');
            redirect('setup/stepone');

        else : 

            $this->session->set_flashdata('message', 'Database Table ' . $db_table_name . ' was not created. Please check DB connection settings and try again.');
            redirect('setup/stepone');

        endif;
    }
}
