<?php
    
class Edit extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function movie($movie) {
        echo 'edit movie screen';
        echo 'editing movie ' . $movie;
    }

    public function users($user) {
        echo 'edit user screen';
        echo 'editing user ' . $user;
    }
}
